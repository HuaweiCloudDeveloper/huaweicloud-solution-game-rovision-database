[TOC]

**解决方案介绍**
===============
该解决方案通过华为云ECS+GaussDB(for Redis) + DDS+RDS for MySQL服务组合，解决游戏客户新游戏上线期间，批量购买数据库流程繁琐、费时费力等问题，提高客户开服效率，提升游戏客户体验。

解决方案实践详情页面地址：https://www.huaweicloud.com/solution/implementations/game-rovision-database.html

**架构图**
---------------
![方案架构](./document/game-rovision-database.png)

**架构描述**
---------------
该解决方案会部署如下资源：
- 创建多台弹性云服务器 ECS，用于安装部署游戏服。
- 创建一台云数据库 GaussDB(for Redis)实例，用于保存KV数据。
- 创建多台文档数据库服务 DDS，用于保存游戏装备、用户积分和游戏日志等非结构化数据。
- 创建多台云数据库 RDS for MySQL，用于保存游戏交易相关的业务数据。

**组织结构**
---------------

``` lua
huaweicloud-solution-game-rovision-database
├──game-rovision-database.tf.json -- 资源编排模板

```
**开始使用**
---------------

1.登录[华为云ECS控制台](https://console.huaweicloud.com/ecm/?agencyId=8f3a7568dba64651869aa83c1b53de79&region=cn-north-4&locale=zh-cn#/ecs/manager/vmList)，进入ECS列表。

图1 ECS控制台

![ECS控制台](./document/readme-image-001.png)

2.查看创建的ECS基本信息。

图2 ECS的基本信息

![ECS的基本信息](./document/readme-image-002.png)

3.登录[云数据库RDS for MySQL](https://console.huaweicloud.com/rds/?agencyId=8f3a7568dba64651869aa83c1b53de79&region=cn-north-4&locale=zh-cn#/rds/management/list)服务控制台，查看实例管理列表。

图3 云数据库 (for MySQL)控制台

![云数据库 (for MySQL)控制台](./document/readme-image-003.png)

4.查看创建的RDS for MySQL基本信息、读写内网地址和端口。

图4 云数据库(for MySQL)读写内网地址和端口

![云数据库(for MySQL)读写内网地址和端口](./document/readme-image-004.png)

5.登录华为云[GaussDB(for Redis)](https://console.huaweicloud.com/nosql/?agencyId=8f3a7568dba64651869aa83c1b53de79&region=cn-north-4&locale=zh-cn#/instance/list)服务控制台，查看实例管理列表。

图5 GaussDB (for Redis)控制台

![GaussDB (for Redis)控制台](./document/readme-image-005.png)

6.查看创建GaussDB(for Redis)的负载均衡地址和端口。

图6 GaussDB (for Redis)负载均衡地址和端口

![GaussDB (for Redis)负载均衡地址和端口](./document/readme-image-006.png)


7.登录华为云[文档数据库DDS](https://console.huaweicloud.com/dds/?agencyId=8f3a7568dba64651869aa83c1b53de79&region=cn-north-4&locale=zh-cn#/dds/management/list)服务控制台，查看实例管理列表。

图7 文档数据库DDS控制台

![文档数据库DDS控制台](./document/readme-image-007.png)

8.查看创建文档数据库DDS的高可用连接地址和端口。

图8 文档数据库DDS的高可用连接地址和端口

![文档数据库DDS的高可用连接地址和端口](./document/readme-image-008.png)

9.在游戏服配置文件里配上GaussDB(for Redis)、DDS和RDS for MySQL的IP和端口，用于业务连接数据库。


